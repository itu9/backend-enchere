package extract.vol;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import extract.avion.ClasseMap;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Table(name = "vprixclasse")
@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class ClassPrix extends ClasseMap {
    double prix;
    double promotion;

    @JsonIgnore
    @JoinColumn(name = "volid")
    @ManyToOne
    Vol vol;

    public int getLibre() {
        return 10;
    }
}

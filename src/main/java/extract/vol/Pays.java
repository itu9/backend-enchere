package extract.vol;

import javax.persistence.Entity;

import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Pays extends HElement<Integer> {
    String nom;
}

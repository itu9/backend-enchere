package extract.vol;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;

@CrossOrigin(value = "*")
@RequestMapping("/vol")
@RestController
public class VolCnt extends CrudController<Vol,Integer,VolRepos,VolService> {

    @Autowired
    public VolCnt(VolService service) {
        super(service);
    }
    
}

package extract.vol;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VolRepos extends JpaRepository<Vol,Integer> {
    
}

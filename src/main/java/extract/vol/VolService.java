package extract.vol;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.model.HCrud;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@Service
public class VolService extends HCrud<Vol, Integer, VolRepos> {

    @Autowired
    public VolService(VolRepos repos) {
        super(repos);
    }

}

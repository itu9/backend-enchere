package extract.vol;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import extract.avion.Avion;
import extract.avion.Classe;
import extract.avion.TypePLace;
import extract.model.HElement;
import extract.ticket.Place;
import extract.ticket.PlaceMap;
import extract.ticket.Simba;
import extract.ticket.passages.PassageRepos;
import extract.ticket.passages.Passages;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Vol extends HElement<Integer> {
    String nom;

    Date pdebut;
    Date pfin;

    @JoinColumn(name = "avionid")
    @ManyToOne
    Avion avion;

    @JoinColumn(name = "depart")
    @OneToOne
    Pays depart;

    @JoinColumn(name = "arrive")
    @OneToOne
    Pays arrive;

    @OneToMany(mappedBy = "vol", cascade = CascadeType.ALL)
    List<ClassPrix> prixclasses;

    /**
     * TypePlace mila arrangena le 1 hoe le classe click
     * Fonction recursive
     * Allouena louh le place vo antsoina ity fonction ity
     * Mila alaina avy any amin'ny base louh le vol
     * Axe :
     * --> siege
     * |
     * v rang
     * 
     * @throws NoIndex
     **/
    public void findPlaces(Place place, TypePLace[] typePlaces,
            int current, List<Place> result, List<PlaceMap> alloc, PassageRepos repos, List<PlaceMap> secours, List<PlaceMap> simba)
            throws NoIndex {
        List<Passages> passages = this.filter(typePlaces[current], repos); // Maka ny passagers apetraka eo amin'ny
                                                                           // typeplace
        int[] rang = this.extractR(typePlaces[current]);// 0 rangmin, 1 rangmax
        int siege = this.getAvion().getRang() - 1; // --> siege ex : A -> F
        // int[] siegei = new int[] { 0, 0 }; // incrementation suivant x
        int[][] places = this.arrange(this.affFreePlace(alloc, rang, siege, place.getSiege(), place.getRang()));
        int index = 0;
        while (passages.size() > 0) {
            /** mitady mandrapa tapitra an le siege na lany le olona **/
            // this.alloc(passages, (place.getRang() + siegei[1]), (place.getSiege() + siegei[0]), alloc, secours, result);
            this.alloc(passages, (places[index][1]), (places[index][0]), alloc, secours,simba, result);
            index++;
            // try {
            //     this.nextPlace(new int[] { place.getSiege(), place.getRang() }, siegei, new int[] { 0, rang[0] },
            //             new int[] { siege, rang[1] });
            // } catch (NoIndex e) {
            //     break;
            // }
        }
        if (!(typePlaces.length > current + 1)) {
            return; // Vita le typeplace rehetra
        }
        Place nPlace = new Place(); // maka ny place akaiky indrindra am le classe manaraka
        nPlace.setSiege(place.getSiege()); // siege atao mifanintsy foana
        int[] nRang = this.extractR(typePlaces[current + 1]);
        if (typePlaces[current + 1].getValue() < typePlaces[current].getValue()) {
            nPlace.setRang(nRang[1]);
            /*
             * rah manana valeur ambany le manaraka de rang max no rang an le place manaraka
             */
        } else {
            nPlace.setRang(nRang[0]);
            /*
             * rah manana valeur ambany le manaraka de rang min no rang an le place manaraka
             */
        }
        this.findPlaces(nPlace, typePlaces, current + 1, result, alloc, repos, secours,simba);
    }

    public int[][] arrange(List<double[]> liste) {
        int[][] ans = new int[liste.size()][];
        double max = liste.get(this.min(liste, false))[2];
        for (int i = 0; i < ans.length; i++) {
            int min = this.min(liste, true);
            double d[] = liste.get(min);
            ans[i] = new int[] {(int)(d[0]), (int)(d[1])};
            d[2] = max;
        }
        return ans;
    }

    public int min(List<double[]> liste, boolean verif) {
        int min = 0;
        for (int i = 1; i < liste.size(); i++) {
            if ((liste.get(i)[2] < liste.get(min)[2]) == verif) {
                min = i;
            }
        }
        return min;
    }

    public double distance(int rang, int siege, int x, int y) {
        double xval = (x - siege) * (x - siege);
        double yval = (y - rang) * (y - rang);
        return Math.sqrt(xval + yval);
    }

    public List<double[]> affFreePlace(List<PlaceMap> alloc, int[] rang, int siege, int x, int y) {
        List<double[]> ans = new ArrayList<double[]>();
        int irange = rang[0];
        while (irange <= rang[1]) {
            for (int i = 0; i <= siege; i++) {
                if (!this.estAllouer(irange, i, alloc)) {
                    ans.add(new double[] { i, irange, this.distance(irange, i, x, y) });
                }
            }
            irange++;
        }
        return ans;
    }

    public void nextPlace(int[] rang, int[] rangj, int[] rangmin, int[] rangmax) throws NoIndex {
        try {
            rangj[0] = this.nextPlace(rang[0], rangj[0], rangmin[0], rangmax[0]);
        } catch (NoIndex e) {
            rangj[0] = 0;
            rangj[1] = this.nextPlace(rang[1], rangj[1], rangmin[1], rangmax[1]);
        }
    }

    public int nextPlace(int rang, int rangj, int rangmin, int rangmax) throws NoIndex {
        rangj *= (-1);
        if (rangj >= 0) {
            rangj++;
        }
        this.isOver(rang, rangj, rangmin, rangmax);
        if ((rangj >= 0 && (rangj + rang) > rangmax) || (rangj < 0 && (rangj + rang) < rangmin)) {
            return this.nextPlace(rang, rangj, rangmin, rangmax);
        }
        return rangj;
    }

    public void isOver(int rang, int rangj, int rangmin, int rangmax) throws NoIndex {
        int value = Math.abs(rangj);
        if (rangmax < (value + rang) && (value - rang) < rangmin) {
            throw new NoIndex();
        }
    }

    public void alloc(List<Passages> passages, int rang, int siege, List<PlaceMap> alloc,
            List<PlaceMap> secours,List<PlaceMap> simba, List<Place> result) {
        if (this.estAllouer(rang, siege, alloc) || this.estAllouer(rang, siege, simba)) {
            return;
        }
        if (!this.estsecours(rang, siege, secours)) {
            Passages mineur = this.extractMineur(passages);
            this.alloc(passages, result, mineur, rang, siege);
        } else {
            try {
                Passages majeur = this.extractMajeur(passages);
                this.alloc(passages, result, majeur, rang, siege);
            } catch (NoMajeur e) {
            }
        }
    }

    public boolean estsecours(int rang, int siege, List<PlaceMap> alloc) {
        for (int i = 0; i < alloc.size(); i++) {
            if (alloc.get(i).getRang() == rang) {
                return true;
            }
        }
        return false;
    }

    public void alloc(List<Passages> passages, List<Place> result, Passages passage, int rang, int siege) {
        Place place = new Place();
        place.setRang(rang);
        place.setSiege(siege);
        place.setPassages(passage);
        result.add(place);
        passages.remove(passage);
    }

    public Passages extractPassage(List<Passages> passages, boolean verif) {
        for (int i = 0; i < passages.size(); i++) {
            if (passages.get(i).estMineur() == verif) {
                return passages.get(i);
            }
        }
        if (passages.size() == 0) {
            return null;
        }
        return passages.get(0);
    }

    public Passages extractMineur(List<Passages> passages) {
        return this.extractPassage(passages, true);
    }

    public Passages extractMajeur(List<Passages> passages) throws NoMajeur {
        Passages passage = this.extractPassage(passages, false);
        if (passage == null) {
            throw new NoMajeur();
        }
        if (passage.estMineur()) {
            throw new NoMajeur();
        }
        System.out.println("Majeur : " + passage);
        return passage;
    }

    public boolean estAllouer(int rang, int siege, List<PlaceMap> alloc) {
        for (int i = 0; i < alloc.size(); i++) {
            if (alloc.get(i).getRang() == rang && alloc.get(i).getSiege() == siege) {
                return true;
            }
        }
        return false;
    }

    public List<Passages> filter(TypePLace type, PassageRepos pRepos) {
        return pRepos.findByTypeplaceid(type.getId());
    }

    public Classe filterClasse(TypePLace type) {
        for (int i = 0; i < this.getAvion().getClasses().size(); i++) {
            if (type.getId() == this.getAvion().getClasses().get(i).getType().getId()) {
                return this.getAvion().getClasses().get(i);
            }
        }
        return null;
    }

    public int[] extractR(TypePLace type) {
        Classe classe = this.filterClasse(type);
        if (classe != null) {
            return new int[] { classe.getRDebut(), classe.getRFin() };
        }
        return new int[] { -1, -1 };
    }

}

package extract.avion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;

@CrossOrigin(value = "*")
@RestController
@RequestMapping("/type")
public class TypePlaceCnt extends CrudController<TypePLace,Integer,TypePlaceRepos,TypePlaceService>{

    @Autowired
    public TypePlaceCnt(TypePlaceService service) {
        super(service);
        //TODO Auto-generated constructor stub
    }
    
}

package extract.avion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;
import lombok.Data;
import lombok.EqualsAndHashCode;

@CrossOrigin(value = "*")
@RequestMapping("/avion")
@RestController
@Data
@EqualsAndHashCode(callSuper = false)
public class AvionCnt extends
        CrudController<Avion, Integer, AvionRepos, AvionService> {
    
    @Autowired
    public AvionCnt(AvionService service) {
        super(service);
    }
}

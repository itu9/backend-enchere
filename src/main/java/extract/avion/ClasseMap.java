package extract.avion;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@MappedSuperclass
public class ClasseMap extends HElement<Integer>{
    @JoinColumn(name = "typeplaceid")
    @OneToOne
    TypePLace type;
    @Column(name = "rdebut")
    int rDebut;

    @Column(name = "rfin")
    int rFin;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "avionid")
    Avion avion ;
}

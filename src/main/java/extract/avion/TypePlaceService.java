package extract.avion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.model.HCrud;

@Service
public class TypePlaceService extends HCrud<TypePLace, Integer, TypePlaceRepos> {

    @Autowired
    public TypePlaceService(TypePlaceRepos repos) {
        super(repos);
    }

}

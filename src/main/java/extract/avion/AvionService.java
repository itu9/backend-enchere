package extract.avion;

import org.springframework.stereotype.Service;

import extract.model.HCrud;

@Service
public class AvionService extends HCrud<Avion, Integer,AvionRepos> {

    public AvionService(AvionRepos repos) {
        super(repos);
    }
    
}

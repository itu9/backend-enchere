package extract.avion;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Avion extends HElement<Integer> {
    String nom;
    int rang;
    int siege;

    @OneToMany(mappedBy = "avion",cascade = CascadeType.ALL)
    List<Classe> classes;
}

package extract.avion;

import javax.persistence.Entity;
import javax.persistence.Table;

import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Table(name = "typeplace")
@Entity
@EqualsAndHashCode(callSuper = false)
public class TypePLace extends HElement<Integer> {
    String description;
    int value;
}

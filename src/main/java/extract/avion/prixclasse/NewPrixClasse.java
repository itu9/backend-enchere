package extract.avion.prixclasse;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "newprixclasse")
@Data
@EqualsAndHashCode(callSuper = false)
public class NewPrixClasse extends HElement<Integer> {

    @ManyToOne()
    @JoinColumn(name = "prixclasseid")
    PrixClasse prixClasse;

    double prix;

    int etat;

}

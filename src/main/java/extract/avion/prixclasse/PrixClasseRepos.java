package extract.avion.prixclasse;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import extract.vol.Vol;

public interface PrixClasseRepos extends JpaRepository<PrixClasse,Integer> {
    public List<PrixClasse> findByVol(Vol vol);
}

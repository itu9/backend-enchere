package extract.avion.prixclasse;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;
import extract.vol.Vol;

@CrossOrigin(value = "*")
@RestController
@RequestMapping(value ="/prixclasse")
public class PrixClasseCnt extends CrudController<PrixClasse,Integer,PrixClasseRepos,PrixClasseService> {

    @Autowired
    public PrixClasseCnt(PrixClasseService service) {
        super(service);
    }

    @PostMapping("/avion")
    public ResponseEntity<?> getByVol(@RequestBody Vol vol) {
        try {
            List<PrixClasse> vols = this.getService().getRepos().findByVol(vol);
            return this.returnSuccess(vols, HttpStatus.OK);
        } catch (Exception e) {
            return this.returnError(e, HttpStatus.NOT_FOUND);
        }
    }
    
}

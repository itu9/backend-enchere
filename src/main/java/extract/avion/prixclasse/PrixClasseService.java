package extract.avion.prixclasse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.model.HCrud;

@Service
public class PrixClasseService extends HCrud<PrixClasse,Integer,PrixClasseRepos> {

    @Autowired
    public PrixClasseService(PrixClasseRepos repos) {
        super(repos);
    }
    
}

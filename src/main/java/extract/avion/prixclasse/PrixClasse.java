package extract.avion.prixclasse;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import extract.avion.Classe;
import extract.model.HElement;
import extract.vol.Vol;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Table(name = "prixclasse")
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class PrixClasse extends HElement<Integer> {
    @OneToOne
    @JoinColumn(name = "classeid")
    Classe classe;

    @OneToOne
    @JoinColumn(name = "volid")
    Vol vol;

    double prix;

    double promotion;
    
    @OneToMany(mappedBy = "prixClasse",cascade = CascadeType.ALL)
    List<NewPrixClasse> price;
    
}

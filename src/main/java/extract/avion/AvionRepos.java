package extract.avion;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AvionRepos extends JpaRepository<Avion, Integer> {

}

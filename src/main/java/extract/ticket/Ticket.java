package extract.ticket;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import extract.auth.Login;
import extract.model.HElement;
import extract.vol.Vol;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Ticket extends HElement<Integer> {
    @JoinColumn(name = "clientid")
    @OneToOne
    Login client;

    @JoinColumn(name = "volid")
    @OneToOne
    Vol vol;

    Date date;

    @OneToMany(mappedBy = "ticket",cascade = CascadeType.ALL)
    List<Ticketclasse> details;

    public Ticket() {
        Calendar cal = Calendar.getInstance();
        this.setDate(new Date(cal.getTime().getTime()));
    }
   
    public void setDetails(List<Ticketclasse> details) {
        for (int i = 0; i < details.size(); i++) {
            details.get(i).setTicket(this);
        }
        this.details = details;
    }

    // int effectif;

    // int prixclasseid;

    // @JsonIgnore
    // @OneToMany(cascade = CascadeType.ALL, mappedBy = "ticket")
    // List<Place> places;
}

package extract.ticket;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonAnyGetter;

import extract.avion.TypePLace;
import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Ticketclasse extends HElement<Integer> {


    @JsonAnyGetter
    @JoinColumn(name = "ticketid")
    @ManyToOne
    Ticket ticket;

    int effectif;

    int prixclasseid;

    @JoinColumn(name = "typeplaceid")
    @OneToOne
    TypePLace type;


    
}

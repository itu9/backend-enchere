package extract.ticket.demandeplace;

import extract.ticket.Place;
import extract.ticket.Ticket;
import lombok.Data;

@Data
public class PlaceDemande {
    Ticket ticket;
    Place place;

    
}

package extract.ticket.demandeplace;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.Cnt;
import extract.ticket.Place;
import lombok.Data;
import lombok.EqualsAndHashCode;

import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("*")
@RequestMapping("/demandeplace")
@RestController
@Data
@EqualsAndHashCode(callSuper = false)
public class PlaceDemandeCnt extends Cnt {
    @Autowired
    PlaceDemandeService pDemandeService;

    @PostMapping("")
    public ResponseEntity<?> demandePlace(@RequestBody PlaceDemande pDemande) {
        try {
            List<Place> ans = this.getPDemandeService().getPlaceList(pDemande);
            return this.returnSuccess(ans, HttpStatus.OK);
        } catch (Exception e) {
            return this.returnError(e, HttpStatus.NOT_FOUND);
        }
    }
}

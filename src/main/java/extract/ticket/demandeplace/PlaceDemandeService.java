package extract.ticket.demandeplace;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.avion.TypePLace;
import extract.avion.TypePlaceService;
import extract.ticket.Place;
import extract.ticket.PlaceMap;
import extract.ticket.PlaceRepos;
import extract.ticket.PlaceSecoursRepos;
import extract.ticket.SimbaRepos;
import extract.ticket.passages.PassageRepos;
import extract.vol.NoIndex;
import extract.vol.Vol;
import extract.vol.VolService;
import lombok.Data;

@Data
@Service
public class PlaceDemandeService {

    @Autowired
    PlaceRepos placeRepos;

    @Autowired
    VolService volService;

    @Autowired
    TypePlaceService tPlaceService;

    @Autowired
    PlaceSecoursRepos pSecoursRepos;

    @Autowired
    PassageRepos passageRepos;

    @Autowired
    SimbaRepos simbaRepos;

    public List<Place> getPlaceList(PlaceDemande pDemande) throws NoIndex {
        Place place = pDemande.getPlace();
        Vol vol = this.getVolService().findById(pDemande.getTicket().getVol().getId());
        TypePLace[] typePlace = this.arrangeAll(pDemande);
        List<Place> result = new ArrayList<>();
        List<PlaceMap> alloc = this.toPlaceMap(this.getPlaceRepos().findByVolid(vol.getId()));
        List<PlaceMap> secours = this
                .toPlaceMap(this.getPSecoursRepos().findByAvionid(vol.getAvion().getId()));
        List<PlaceMap> simba = this
                .toPlaceMap(this.getSimbaRepos().findByAvionid(vol.getAvion().getId()));
        System.out.println(secours);
        vol.findPlaces(place, typePlace, 0, result, alloc, passageRepos, secours,simba);
        return result;
    }

    public List<PlaceMap> toPlaceMap(List<?> liste) {
        List<PlaceMap> result = new ArrayList<PlaceMap>();
        for (int i = 0; i < liste.size(); i++) {
            PlaceMap map = (PlaceMap) liste.get(i);
            result.add(map);
        }
        return result;
    }

    public TypePLace[] arrangeAll(PlaceDemande pDemande) {
        List<TypePLace> ans = this.getTPlaceService().getAll();
        for (int i = 1; i < ans.size(); i++) {
            if (ans.get(i).getId() == pDemande.getPlace().getType().getId()) {
                TypePLace temp = ans.get(i);
                ans.set(i, ans.get(0));
                ans.set(0, temp);
            }
        }
        TypePLace[] res = new TypePLace[ans.size()];
        for (int i = 0; i < res.length; i++) {
            res[i] = ans.get(i);
        }
        return res;
    }
}

package extract.ticket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;
import extract.vol.Vol;

@CrossOrigin("*")
@RequestMapping( "/placeticket")
@RestController
public class PlaceTicketCnt extends CrudController<PlaceTicket,Integer,PlaceTicketRepos,PlaceTicketSer> {

    @Autowired
    public PlaceTicketCnt(PlaceTicketSer service) {
        super(service);
    }

    @GetMapping("/byvol/{id}")
    public ResponseEntity<?> getByVol(@PathVariable("id") int id) {
        try {
            Vol vol = new Vol();
            vol.setId(id);
            return this.returnSuccess(this.getService().getRepos().findByVol(vol), HttpStatus.OK);
        } catch (Exception e) {
            return this.returnError(e, HttpStatus.NOT_FOUND);
        }
    }
    
}

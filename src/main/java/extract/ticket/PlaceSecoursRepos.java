package extract.ticket;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaceSecoursRepos extends JpaRepository<PlaceSecours, Integer> {
    public List<PlaceSecours> findByAvionid(int avionid);
}

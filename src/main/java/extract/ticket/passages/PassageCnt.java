package extract.ticket.passages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;

@CrossOrigin(value = "*")
@RestController
@RequestMapping("/passage")
public class PassageCnt extends CrudController<Passages,Integer,PassageRepos,PassageSer> {

    @Autowired
    public PassageCnt(PassageSer service) {
        super(service);
    }

    @GetMapping("/ticketclasse/{id}")
    public ResponseEntity<?> getByTicketClasse(@PathVariable("id") int id) {
        try {
            return this.returnSuccess(this.getService().getRepos().findByTicketclasseid(id), HttpStatus.OK);
        } catch (Exception e) {
            return this.returnError(e, HttpStatus.NOT_FOUND);
        }
    }
    
}

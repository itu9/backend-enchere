package extract.ticket.passages;
import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;

import javax.persistence.Column;
import javax.persistence.Entity;
import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Passages extends HElement<Integer> {
    String nom;
    Date naissance;

    // @OneToOne
    // @JoinColumn(name = "ticketclasseid")
    // Ticketclasse ticketclasse;
    int ticketclasseid;

    @Column(insertable = false)
    int typeplaceid;

    int estalloue;

    public int getAge() {
        return Period.between(this.getNaissance().toLocalDate(), LocalDate.now()).getYears();
    }

    public boolean isalloc() {
        return this.getEstalloue() == 50;
    }

    public boolean estMineur() {
        return this.getAge() <= 16;
    }
}

package extract.ticket.passages;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PassageRepos extends JpaRepository<Passages,Integer> {
    

    public List<Passages> findByTicketclasseid(int ticketclasseid);
    
    public List<Passages> findByTypeplaceid(int typeplaceid);

}

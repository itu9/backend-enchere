package extract.ticket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/places")
@RestController
public class PlaceCnt extends CrudController<Place,Integer,PlaceRepos,PlaceSer> {

    @Autowired
    public PlaceCnt(PlaceSer service) {
        super(service);
    }
    
}

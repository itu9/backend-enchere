package extract.ticket;

import javax.persistence.MappedSuperclass;

import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
public class PlaceMap extends HElement<Integer> {
    int rang;
    int siege;

}

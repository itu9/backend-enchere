package extract.ticket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;

@CrossOrigin(value = "*")
@RequestMapping("/ticket")
@RestController
public class TicketCnt extends CrudController<Ticket,Integer,TicketRepos,TicketSer> {

    @Autowired
    public TicketCnt(TicketSer service) {
        super(service);
    }
    
}

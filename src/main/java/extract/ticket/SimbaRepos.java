package extract.ticket;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SimbaRepos extends JpaRepository<Simba,Integer> {
    public List<Simba> findByAvionid(int avionid);
    
}

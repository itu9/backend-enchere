package extract.ticket;

import java.util.List;

import javax.persistence.Entity;
import javax.xml.ws.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.CrudController;

@CrossOrigin(value = "*")
@RequestMapping("/placesecours")
@RestController
public class PlaceSecoursCnt extends CrudController<PlaceSecours, Integer, PlaceSecoursRepos, PlaceSecoursSer> {

    @Autowired
    public PlaceSecoursCnt(PlaceSecoursSer service) {
        super(service);
        // TODO Auto-generated constructor stub
    }

    @GetMapping("/avion/{avionid}")
    public ResponseEntity<?> getPlace(@PathVariable("avionid") int ticketid) {
        try {
            return this.returnSuccess(this.getService().getRepos().findByAvionid(ticketid), HttpStatus.OK);
        } catch (Exception e) {
            return this.returnError(e, HttpStatus.NOT_FOUND);
        }
    }

}

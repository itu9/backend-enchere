package extract.ticket.promotion;

import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import extract.avion.TypePLace;
import extract.model.HElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "vpromotion")
@Data
@EqualsAndHashCode(callSuper = false)
public class Promotion extends HElement<Integer> {
    double prix;

    double promotion;

    int effectif;

    Date date;

    int ticketid;

    int enfant;

    int adulte;

    @OneToOne
    @JoinColumn(name = "typeplaceid")
    TypePLace type;  

    public double getValue() {
        return this.getNprix() - this.getRemise();
    }

    public double getRemise() {
        return (this.getNprix() * this.getPromotion() / 100.0);
    }

    public boolean getEstSomme() {
        return this.getId() == null;
    }

    public double getNprix() {
        int eff = 0;
        if (this.getAdulte() == 0) {
            eff = this.getEffectif();
        } else {
            eff = this.getAdulte();
        }
        return this.getPrix() * eff;
    }
}

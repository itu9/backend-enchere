package extract.ticket.promotion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import extract.controller.CrudController;

@CrossOrigin(value = "*")
@RequestMapping("/promotion")
@RestController
public class PromotionCnt extends CrudController<Promotion, Integer, PromotionRepos, PromotionSer> {

    @Autowired
    public PromotionCnt(PromotionSer service) {
        super(service);
        // TODO Auto-generated constructor stub
    }

    @GetMapping("/ticket/{ticketid}")
    public ResponseEntity<?> getPromByTicket(@PathVariable("ticketid") int ticketid) {
        try {
            return this.returnSuccess(this.getService().getByTicket(ticketid), HttpStatus.OK);
        } catch (Exception e) {
            return this.returnError(e, HttpStatus.NOT_FOUND);
        }
    }

}

package extract.ticket.promotion;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PromotionRepos extends JpaRepository<Promotion,Integer>{
    public List<Promotion> findByTicketid(int ticketid);
}

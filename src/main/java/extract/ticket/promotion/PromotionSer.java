package extract.ticket.promotion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.model.HCrud;

@Service
public class PromotionSer extends HCrud<Promotion,Integer,PromotionRepos> {

    @Autowired
    public PromotionSer(PromotionRepos repos) {
        super(repos);
        //TODO Auto-generated constructor stub
    }

    public void sum(List<Promotion> liste) {
        Promotion p = new Promotion();
        for (int i = 0; i < liste.size(); i++) {
            p.setPrix(liste.get(i).getNprix()+p.getPrix());
            
        }
        liste.add(p);
    }

    public List<Promotion> getByTicket(int ticketid) {
        List<Promotion> ans = this.getRepos().findByTicketid(ticketid);
        this.sum(ans);
        return ans;
    }
    
}

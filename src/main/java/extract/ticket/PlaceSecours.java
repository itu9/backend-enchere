package extract.ticket;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@ToString
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "placesecours")
public class PlaceSecours extends PlaceMap {
    int avionid;
}

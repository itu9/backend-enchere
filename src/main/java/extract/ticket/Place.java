package extract.ticket;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import extract.avion.TypePLace;
import extract.model.HElement;
import extract.ticket.passages.Passages;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Place extends PlaceMap {

    @OneToOne()
    @JoinColumn(name = "typeplaceid",nullable = true)
    TypePLace type;

    @JoinColumn(name = "ticketclasseid")
    @ManyToOne()
    Ticketclasse ticket;

    @OneToOne
    @JoinColumn(name = "passageid")
    Passages passages;

    int volid;

}

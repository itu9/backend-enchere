package extract.ticket;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaceRepos extends JpaRepository<Place,Integer> {
    public List<Place> findByVolid(int volid);
}

package extract.ticket;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TicketRepos extends JpaRepository<Ticket,Integer> {
    
}

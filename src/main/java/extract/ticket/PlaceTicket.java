package extract.ticket;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import extract.auth.Login;
import extract.model.HElement;
import extract.vol.Vol;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "vplace")
@Entity
public class PlaceTicket extends HElement<Integer> {
    int rang;
    int siege;
    int typeplaceid;

    @JoinColumn(name = "ticketid")
    @ManyToOne()
    Ticket ticket;

    @JoinColumn(name = "volid")
    @ManyToOne()
    Vol vol;

    @JoinColumn(name = "clientid")
    @ManyToOne()
    Login client;

    int effectif;

    Date date;

    int etat;
}

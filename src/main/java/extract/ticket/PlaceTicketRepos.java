package extract.ticket;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import extract.vol.Vol;

public interface PlaceTicketRepos extends JpaRepository<PlaceTicket, Integer> {
    public List<PlaceTicket> findByVol(Vol vol);
}
